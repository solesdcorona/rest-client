package rhtotal.rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rhtotal.client.ClientInvoke;
import rhtotal.client.builder.ClientBuilder;
import rhtotal.to.InvokeTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@Path("clientinvoke")
public class ClientInvokeResource {


    private static final Logger logger = LogManager.getLogger(ClientInvokeResource.class);


    private Gson gson = new Gson();


    @POST
    @Path("/sico")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response newWebex(InvokeTO meeting) {
        logger.info("Sico {}", meeting);
        String payload = "error";
        String message = "";
        try {
            String url = "https://nominaenlanube.com:8085/api-nomen/v1/intelplan/auth";
            String auth = "?user=%s&password=%s";
            final String[] json = new String[1];
            Optional.ofNullable(meeting.getPayload()).ifPresent((a)->{
                Type mapType = new TypeToken<Map<String, String>>() {}.getType();
                json[0] =gson.toJson(meeting.getPayload(),mapType);
            });

            ClientBuilder.Builder clientBuilder = ClientBuilder.newClientBuilder()
                    .user(meeting.getUser())
                    .password("20Api18_$4x1t1_1nt3l").method(meeting.getMethod()).requestPayload(json[0])
                    .urlAuthorization(url.concat(String.format(auth, "api_trabajador", "20Api18_$4x1t1_1nt3l")))
                    .urlTarget(meeting.getUrl());

            Consumer<ClientBuilder.Builder> consumerAuth = (y) ->
            {
                logger.info("client {}",y);

                JsonObject jsonObj = gson.fromJson(y.getRespondAuthorization(), JsonObject.class);
                jsonObj = jsonObj.get("token").getAsJsonObject();
                String token = jsonObj.get("token_acceso").getAsString();
                y.token(token);
            };
            Consumer<ClientBuilder.Builder> consumerApi = (y) ->
            {

                String token = y.getToken();
                String response = y.getRespondeApiInvokation();
                response = response.substring(1, response.length() - 1);
                String key = token.substring(token.length() - 8, token.length());
                logger.info("key {}", key);
                y.keyToDesencrypt(key);
                y.payloadToDencrypt(response);
                logger.info("response {}", response);
            };
            ClientInvoke invokeRequest = clientBuilder.build();
                invokeRequest.tokenAuthorization(consumerAuth)
                        .invokeRestApi(consumerApi)
                        .descryptPayLoad();
            String response = clientBuilder.getResponseDesencrypt();

            logger.info("json {}", response);
            return Response.ok(response).build();
        } catch (Exception e) {
            e.printStackTrace();
            message=e.getMessage();
        }
        return Response.ok(message).build();
    }


    @POST
    @Path("/swap")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response newSwap(InvokeTO meeting) {
        logger.info("SWAP {}", meeting);
        String payload = "error";
        try {
            Map<String, String> headers= new HashMap<>(meeting.getHeaders());
            ClientBuilder.Builder clientBuilder = ClientBuilder.newSwapBuilder().addHeaders(headers)
                    .urlTarget(meeting.getUrl());
            Type mapType = new TypeToken<Map<String, String>>() {}.getType();
            String json =gson.toJson(meeting.getPayload(),mapType);
            logger.info("SWAP-PAYLOAD {}", json);
            clientBuilder.requestPayload(json);
            clientBuilder.build().invokeRestApi(null);
            String response = clientBuilder.getRespondeApiInvokation();
            logger.info("json {}", response);
            return Response.ok(response).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok("error").build();
    }

}
