package rhtotal.client.builder;

import rhtotal.client.ClientInvoke;
import rhtotal.client.impl.ClientInvokeImpl;
import rhtotal.client.impl.ClientSwapInvokeImpl;

import java.util.HashMap;
import java.util.Map;

public abstract class ClientBuilder {

    public static Builder newClientBuilder() {
        return new ClientInvokeImpl();
    }

    public static Builder newSwapBuilder() {
        return new ClientSwapInvokeImpl();
    }

    public static abstract class Builder {


        private String user;
        private String password;
        private String method;
        private String urlAuthorization;
        private String urlTarget;
        private String token;
        private String payloadToDesencrypt;
        private String keyToDesencrypt;

        private String requestPayload;
        private Map<String, String> headers= new HashMap<>();

        private String respondeAuthorization;
        private String respondeApiInvokation;
        private String responseDesencrypt;





        public Builder() {
        }

        public String getUrlAuthorization() {
            return urlAuthorization;
        }

        public Builder urlAuthorization(String urlAuthorization) {
            this.urlAuthorization = urlAuthorization;
            return this;
        }

        public String getUrlTarget() {
            return urlTarget;
        }

        public Builder urlTarget(String urlTarget) {
            this.urlTarget = urlTarget;
            return this;
        }

        public String getToken() {
            return token;
        }

        public Builder token(String token) {
            this.token = token;
            return this;
        }

        public String getPayloadToDesencrypt() {
            return payloadToDesencrypt;
        }

        public Builder payloadToDencrypt(String payloadToDesencrypt) {
            this.payloadToDesencrypt = payloadToDesencrypt;
            return this;
        }

        public String getKeyToDecrypt() {
            return keyToDesencrypt;
        }

        public Builder keyToDesencrypt(String keyToDesencrypt) {
            this.keyToDesencrypt = keyToDesencrypt;
            return this;
        }

        public String getUser() {
            return user;
        }

        public Builder user(String user) {
            this.user = user;
            return this;
        }

        public String getPassword() {
            return password;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public String getMethod() {
            return method;
        }

        public Builder method(String method) {
            this.method = method;
            return this;
        }


        // public abstract ClientInvoke build();


        public String getResponseDesencrypt() {
            return responseDesencrypt;
        }

        public Builder setResponseDesencrypt(String responseDesencrypt) {
            this.responseDesencrypt = responseDesencrypt;
            return this;
        }

        public String getRespondeApiInvokation() {
            return respondeApiInvokation;
        }

        public Builder setRespondeApiInvokation(String respondeApiInvokation) {
            this.respondeApiInvokation = respondeApiInvokation;
            return this;
        }

        public String getRespondAuthorization() {
            return respondeAuthorization;
        }

        public Builder setRespondAuthorization(String respondeAuthorization) {
            this.respondeAuthorization = respondeAuthorization;
            return this;
        }


        public abstract ClientInvoke build();

        public String getRequestPayload() {
            return requestPayload;
        }

        public Builder requestPayload(String requestPayload) {
            this.requestPayload = requestPayload;
            return this;
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public Builder addHeaders(Map<String, String> headers) {
            this.headers.putAll(headers);
            return this;
        }
    }
}