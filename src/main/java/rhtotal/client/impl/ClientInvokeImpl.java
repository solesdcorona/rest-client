package rhtotal.client.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rhtotal.client.ClientInvoke;
import rhtotal.client.builder.ClientBuilder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.util.Base64;
import java.util.Optional;
import java.util.function.Consumer;

public class ClientInvokeImpl extends ClientBuilder.Builder implements ClientInvoke {

    private static final Logger logger = LogManager.getLogger(ClientInvokeImpl.class);
    private static final Integer STATUS_CODE = 200;


    public static void main(String... args) {
        Gson gson = new Gson();

        String url = "https://nominaenlanube.com:8085/api-nomen/v1/intelplan/auth";
        String auth = "?user=%s&password=%s";

        ClientBuilder.Builder clientBuilder = ClientBuilder.newClientBuilder()
                .user("api_trabajador")
                .password("20Api18_$4x1t1_1nt3l")
                .urlAuthorization(url.concat(String.format(auth, "api_trabajador", "20Api18_$4x1t1_1nt3l")))
                .urlTarget("https://nominaenlanube.com:8085/api-nomen/v1/get_empleado/laragtz@prueba.com");

        Consumer<ClientBuilder.Builder> consumerAuth = (y) ->
        {
            logger.info("client {}",y);

            JsonObject jsonObj = gson.fromJson(y.getRespondAuthorization(), JsonObject.class);
            jsonObj = jsonObj.get("token").getAsJsonObject();
            String token = jsonObj.get("token_acceso").getAsString();
            y.token(token);
        };
        Consumer<ClientBuilder.Builder> consumerApi = (y) ->
        {

            String token = y.getToken();
            String response = y.getRespondeApiInvokation();
            response = response.substring(1, response.length() - 1);
            String key = token.substring(token.length() - 8, token.length());
            logger.info("key {}", key);
            y.keyToDesencrypt(key);
            y.payloadToDencrypt(response);
            logger.info("response {}", response);
        };
        ClientInvoke invokeRequest = clientBuilder.build();
        try {
            invokeRequest.tokenAuthorization(consumerAuth)
                    .invokeRestApi(consumerApi)
                    .descryptPayLoad();
        }catch (Exception e){
            logger.error("Error al concretar ",e);
        }
        logger.info("json {}", clientBuilder.getResponseDesencrypt());
    }


    @Override
    public ClientInvoke tokenAuthorization(Consumer consumer) throws Exception {

        HttpRequest request = null;
        try {

            Optional.ofNullable(this.getUrlAuthorization()).orElseThrow(()->new Exception("Se debe especificar la url de generar el token"));
            HttpClient client = HttpClient.newBuilder().build();
            request = HttpRequest.newBuilder()
                    .uri(new URI(this.getUrlAuthorization()))
                    .GET()
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());
            setRespondAuthorization(response.body());
            if(STATUS_CODE.equals(response.statusCode())){
                consumer.accept(this);
            }else{
                throw  new Exception("Error al obtener el token SICO");
            }
            return this;
        } catch (Exception e) {
            logger.error("Error al autenticar a Sico ",e);
            throw  e;
        }
    }

    @Override
    public ClientInvoke invokeRestApi(Consumer consumer) throws Exception {
        HttpRequest request = null;
        try {

            Optional.ofNullable(this.getUrlTarget()).orElseThrow(()->new Exception("Se debe especificar la url del servicio"));
            Optional.ofNullable(this.getToken()).orElseThrow(()->new Exception("Se debe especificar el token"));
            String urlTarget = this.getUrlTarget();
            urlTarget=urlTarget.replaceAll("\\s","+");
            //String urlNew = URLEncoder.encode(urlTarget,"UTF-8");
            logger.info(" url tarjet {}",urlTarget);
            HttpClient client = HttpClient.newBuilder().build();
            HttpRequest.Builder buildSico = HttpRequest.newBuilder()
                    .uri(new URI(urlTarget))
                    .header("Authorization", String.format("Bearer %s", getToken()));
            if(getMethod().equals("POST")){
                Optional.ofNullable(this.getRequestPayload()).orElseThrow(()->new Exception("Se debe especificar un payload"));
                HttpRequest.BodyPublisher requestPayload= HttpRequest.BodyPublisher.fromString(this.getRequestPayload());
                buildSico=buildSico.POST(requestPayload);
            } else{
                buildSico=buildSico.GET();
            }
            request =  buildSico.build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());

            if(STATUS_CODE.equals(response.statusCode())){
                setRespondeApiInvokation(response.body());
                consumer.accept(this);
            }else{
                throw  new Exception(String.format("Error al invocar servicio %s Response SICO %s",urlTarget,response.body()));
            }
            return this;
        } catch (Exception e) {
            logger.error("Error al invocar  SICO",e);
            throw  e;
        }
    }

    @Override
    public ClientInvoke descryptPayLoad(Consumer consumer) throws Exception {
        ClientInvoke service = descryptPayLoad();
        consumer.accept(this);
        return service;
    }

    @Override
    public ClientInvoke descryptPayLoad() throws Exception {
        try {

            Optional.ofNullable(this.getKeyToDecrypt()).orElseThrow(()->new Exception("Se debe especificar la llave para desencriptar"));
            Optional.ofNullable(this.getPayloadToDesencrypt()).orElseThrow(()->new Exception("Se debe especificar el Payload"));
            String payload = this.getPayloadToDesencrypt();
            String key = this.getKeyToDecrypt();

            logger.info(" payload {}", payload);
            byte[] decodedPayLoad = Base64.getDecoder().decode(payload);

            SecretKey secKey = new SecretKeySpec(key.getBytes("UTF-8"), "DES");
            System.out.println(secKey);
            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secKey);
            byte[] textDecrypted = cipher.doFinal(decodedPayLoad);
            setResponseDesencrypt(new  String(textDecrypted, "UTF-8"));
            return this;
        } catch (Exception e) {
            logger.error("Error al desencriptara response Sico",e);
            throw  new Exception("Error al desencriptara response Sico",e);
        }
    }


    @Override
    public ClientInvoke build() {
        return this;
    }
}
