package rhtotal.client.impl;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpRequest.BodyPublisher;
import jdk.incubator.http.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import rhtotal.client.ClientInvoke;
import rhtotal.client.builder.ClientBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class ClientSwapInvokeImpl extends ClientBuilder.Builder implements ClientInvoke {


    public static void main(String... args) {

        Map<String, String> headers= new HashMap<>();
        headers.put("content-type","application/json");
        headers.put("x-parse-application-id","PzADDOSNuKxg4KRSmqfCt0jZTnjdmqE3ORBE9Tev");
        headers.put("x-parse-rest-api-key","Jn9zY6qRzdQ067GiiKATpVKLhw5wRM8dReXagIW9");

        ClientBuilder.Builder clientBuilder = ClientBuilder.newSwapBuilder().addHeaders(headers)
                .urlTarget("https://parseapi.back4app.com/functions/transfer").requestPayload("{\n" +
                "\"apiKey\":\"sk_live_rA3iIwwKMX75RmB8az47jH8qWl2nmPBs\",\n" +
                "\"transferId\":\"00004\",\n" +
                "\"description\":\"Abono\",\n" +
                "\"amount\":1.00 ,\n" +
                "\"userId\":\"YmQB57zxLv\",\n" +
                "\"email\":\"solesdcorona@gmail.com\",\n" +
                "\"hone\":\"5532337425\"\n" +
                "}");

        try {
            clientBuilder.build().invokeRestApi(null);
            String jsonResult = clientBuilder.getRespondeApiInvokation();
            logger.info("Response {}",jsonResult);
        }catch (Exception e){
            logger.error("Error service ",e);
        }
        logger.info("json {}", clientBuilder.getRespondeApiInvokation());
    }

    private static final Logger logger = LogManager.getLogger(ClientSwapInvokeImpl.class);

    @Override
    public ClientInvoke tokenAuthorization(Consumer consumer) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public ClientInvoke invokeRestApi(Consumer consumer) throws Exception {
        HttpRequest request = null;
        try {

            Optional.ofNullable(this.getUrlTarget()).orElseThrow(()->new Exception("Se debe especificar la url del servicio"));
            Optional.ofNullable(this.getHeaders().get("x-parse-application-id")).orElseThrow(()->new Exception("Se debe especificar el x-parse-application-id"));
            Optional.ofNullable(this.getHeaders().get("x-parse-rest-api-key")).orElseThrow(()->new Exception("Se debe especificar el x-parse-rest-api-key"));

            String urlTarget = this.getUrlTarget();
            urlTarget=urlTarget.replaceAll("\\s","+");
            //String urlNew = URLEncoder.encode(urlTarget,"UTF-8");
            logger.info(" url tarjet {}",urlTarget);
            logger.info(" Payload  {}",getRequestPayload());
            //consumer.accept(this);
            HttpClient client = HttpClient.newBuilder().build();
            BodyPublisher requestPayload= BodyPublisher.fromString(this.getRequestPayload());
            HttpRequest.Builder builderRequest = HttpRequest.newBuilder()
                    .uri(new URI(urlTarget));

            getHeaders().forEach((k,v)->{
                builderRequest.setHeader(k,v);
            });

            request=builderRequest.POST(requestPayload).build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());
            setRespondeApiInvokation(response.body());

            return this;
        } catch (Exception e) {
            logger.error("Error al invocar a Sico",e);
            throw  e;
        }
    }

    @Override
    public ClientInvoke descryptPayLoad(Consumer consumer) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public ClientInvoke descryptPayLoad() throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public ClientInvoke build() {
        return this;
    }
}
