package rhtotal.client;


import java.util.function.Consumer;

public interface ClientInvoke {

    ClientInvoke tokenAuthorization(Consumer consumer) throws Exception;

    ClientInvoke invokeRestApi(Consumer consumer) throws Exception;

    ClientInvoke descryptPayLoad(Consumer consumer) throws Exception;

    ClientInvoke descryptPayLoad() throws Exception;
}
