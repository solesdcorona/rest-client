module client.micro.profile {
    requires  gson;
    requires  jdk.incubator.httpclient;
    requires  org.apache.logging.log4j;
    requires  javaee.web.api;
}
